<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', fn() => redirect()->route('home'));
Route::get('/login', [AuthController::class, 'loginView'])->name('login.loginView');
Route::post('/login', [AuthController::class, 'login'])->name('login');


// WITH AUTHENTICATION
Route::middleware('auth')->group(function() {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // TRANSACTION
    Route::prefix('/transactions')->name('transactions.')->group(function() {
        Route::get('/', [TransactionController::class, 'index'])->name('index');
    });


    /*
        ADMIN ONLY
    */
    Route::middleware(['admin'])->group(function() {
        // USER
        Route::prefix('/users')->name('users.')->group(function() {
            Route::get('/', [UserController::class, 'index'])->name('index');
            Route::post('/', [UserController::class, 'store'])->name('store');
            Route::put('/{userId}', [UserController::class, 'update'])->name('update');
            Route::delete('/{userId}', [UserController::class, 'destroy'])->name('destroy');
        });
    });


    /*
        SELLER ONLY
    */
    Route::middleware(['seller'])->group(function() {
        Route::prefix('/items')->name('items.')->group(function() {
            Route::get('/', [ItemController::class, 'index'])->name('index');
            Route::post('/', [ItemController::class, 'store'])->name('store');
            Route::put('/{itemId}', [ItemController::class, 'update'])->name('update');
            Route::delete('/{itemId}', [ItemController::class, 'destroy'])->name('destroy');
        });
    });


    /*
        TELLER ONLY
    */
    Route::middleware(['teller'])->group(function() {
        // TRANSACTION
        Route::prefix('/transactions')->name('transactions.')->group(function() {
            Route::prefix('/{transactionId}')->group(function() {
                Route::get('/approve', [TransactionController::class, 'approve'])->name('approve');
                Route::get('/reject', [TransactionController::class, 'reject'])->name('reject');
            });
        });
    });


    /*
        STUDENT ONLY
    */
    Route::middleware(['student'])->group(function() {
        // TRANSACTION
        Route::prefix('/transactions')->name('transactions.')->group(function() {
            Route::post('/topup', [TransactionController::class, 'topup'])->name('topup');
        });
    });
});
